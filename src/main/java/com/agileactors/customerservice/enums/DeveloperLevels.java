package com.agileactors.customerservice.enums;

public enum DeveloperLevels {

    JUNIOR,
    UPPER_JUNIOR,
    MID,
    UPPER_MID,
    SENIOR,
    UPPER_SENIOR,
    PRINCIPAL

}
