package com.agileactors.customerservice.repositories;

import com.agileactors.customerservice.models.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Repository
public class CustomerRepository {

    private final Map<String, Customer> customersMap = new ConcurrentHashMap<>();

    public String save(Customer customer) {
        String customerId = UUID.randomUUID().toString();
        customersMap.put(customerId, customer);
        return customerId;
    }
}
