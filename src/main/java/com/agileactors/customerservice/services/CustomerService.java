package com.agileactors.customerservice.services;

import com.agileactors.customerservice.exceptions.CustomerServiceException;
import com.agileactors.customerservice.models.Customer;
import com.agileactors.customerservice.repositories.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService() {
        this.customerRepository = new CustomerRepository();
    }

    public String addCustomer(Customer customer) {
        if (validateIfCustomerHasData(customer)) {
            return customerRepository.save(customer);
        } else {
            throw new CustomerServiceException("The customer's data are missing");
        }
    }

    private boolean validateIfCustomerHasData(Customer customer) {
        return customer != null
                && !customer.getCompanyName().isEmpty()
                && !customer.getDeveloperLevels().isEmpty();
    }
}