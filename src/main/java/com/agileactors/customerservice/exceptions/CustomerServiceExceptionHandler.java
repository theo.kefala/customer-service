package com.agileactors.customerservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Timestamp;
import java.time.Instant;

@ControllerAdvice
public class CustomerServiceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { CustomerServiceException.class })
    protected ResponseEntity<ErrorMessage> handleCustomerWithNoData(CustomerServiceException ex) {
        ErrorMessage errorMessage = ErrorMessage.builder()
                .date(Timestamp.from(Instant.now()).toString())
                .status(HttpStatus.BAD_REQUEST.toString())
                .message(ex.getMessage())
                .build();
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);

    }

}
