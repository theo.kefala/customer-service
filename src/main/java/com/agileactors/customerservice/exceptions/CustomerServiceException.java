package com.agileactors.customerservice.exceptions;

public class CustomerServiceException extends RuntimeException {
    public CustomerServiceException(String errorMessage) {
        super(errorMessage);
    }
}
