package com.agileactors.customerservice.dtos;

import lombok.Data;

@Data
public class CustomerDto {
    private final Long customerId;
}
