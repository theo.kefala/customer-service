package com.agileactors.customerservice.models;

import com.agileactors.customerservice.enums.DeveloperLevels;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Customer {
    @JsonIgnore
    Long customerId;
    @NonNull
    String companyName;
    @NonNull
    List<DeveloperLevels> developerLevels;

}
