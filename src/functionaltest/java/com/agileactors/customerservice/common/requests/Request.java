package com.agileactors.customerservice.common.requests;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

public interface Request {

    ResponseEntity doExecute(TestRestTemplate testRestTemplate);

}