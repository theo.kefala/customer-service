package com.agileactors.customerservice.common.requests;

import com.agileactors.customerservice.models.Customer;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@Builder
@RequiredArgsConstructor
public class CreateCustomerRequest implements Request {

    private static final String URL = "/api/customers";

    private final Customer customer;

    @Override
    public ResponseEntity<String> doExecute(TestRestTemplate testRestTemplate) {
        HttpEntity<Customer> request = new HttpEntity<>(customer);
        return testRestTemplate.exchange(URL, HttpMethod.POST, request, String.class);
    }
}
