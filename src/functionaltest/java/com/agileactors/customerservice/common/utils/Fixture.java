package com.agileactors.customerservice.common.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

public class Fixture {

    private static final String FIXTURE_ROOT = "fixtures/";

    public static String getMenuItemFromRedis() {
        return loadFileAsString("redis_url_mapping_data.json");
    }

    public static String getUrlMappingWithResult() {
        return loadFileAsString("url_mapping_result.json");
    }

    public static String getUrlMappingWithNearestMatch() {
        return loadFileAsString("url_mapping_nearest_match.json");
    }

    private static String loadFileAsString(String filename) {
        URL source = Fixture.class.getClassLoader().getResource(FIXTURE_ROOT + filename);

        assert source != null;
        try {
            return new String(Files.readAllBytes(new File(source.getFile()).toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
