package com.agileactors.customerservice.common.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

@Data
@Slf4j
public class HeadersRequestInterceptor implements ClientHttpRequestInterceptor {

    private String headerName;
    private String headerValue;

    public HeadersRequestInterceptor(String headerName, String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        httpRequest.getHeaders().set(headerName, headerValue);
        traceRequest(httpRequest, body);
        return execution.execute(httpRequest, body);
    }

    private void traceRequest(HttpRequest request, byte[] body) {
        log.info("===========================  Request Start  ================================================");
        log.info("URI         : {}", request.getURI());
        log.info("Method      : {}", request.getMethod());
        log.info("Headers     : {}", request.getHeaders());
        log.info("Request body: {}", new String(body));
        log.info("===========================   Request End  =================================================");
    }
}
