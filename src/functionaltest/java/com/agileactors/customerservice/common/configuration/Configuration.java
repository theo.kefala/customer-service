package com.agileactors.customerservice.common.configuration;

import com.agileactors.customerservice.common.utils.HeadersRequestInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import javax.annotation.PostConstruct;
import java.util.List;

@TestConfiguration
@AllArgsConstructor
public class Configuration {

    private final TestRestTemplate testRestTemplate;

    private static final String CONTENT_TYPE = "Content-Type";

    @PostConstruct
    public TestRestTemplate initializeRestTemplate() {
        this.testRestTemplate.getRestTemplate()
                .getMessageConverters()
                .removeIf(m -> m.getClass().getName().equals(MappingJackson2HttpMessageConverter.class.getName()));
        this.testRestTemplate.getRestTemplate()
                .getMessageConverters()
                .add(getMappingJackson2HttpMessageConverter());
        this.testRestTemplate.getRestTemplate()
                .setInterceptors(getInterceptors());
        return testRestTemplate;
    }

    private MappingJackson2HttpMessageConverter getMappingJackson2HttpMessageConverter() {
        ObjectMapper objectMapper = new ObjectMapper();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setPrettyPrint(false);
        messageConverter.setObjectMapper(objectMapper);
        return messageConverter;
    }

    private List<ClientHttpRequestInterceptor> getInterceptors() {
        return List.of(new HeadersRequestInterceptor(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));
    }
}
