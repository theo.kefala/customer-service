package com.agileactors.customerservice.common.actors;

import com.agileactors.customerservice.common.requests.CreateCustomerRequest;
import com.agileactors.customerservice.common.requests.Request;
import com.agileactors.customerservice.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class CustomerServiceActor extends Actor {

    @Autowired
    public CustomerServiceActor(TestRestTemplate testRestTemplate) {
        super(testRestTemplate);
    }

    public ResponseEntity<String> postCustomer(Customer customer) {
        Request request = CreateCustomerRequest.builder().customer(customer).build();
        return submitRequest(request);
    }
}
