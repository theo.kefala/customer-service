package com.agileactors.customerservice.common.actors;

import com.agileactors.customerservice.common.requests.Request;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

@RequiredArgsConstructor
public abstract class Actor {

    private final TestRestTemplate testRestTemplate;

    ResponseEntity submitRequest(Request request) {
        return request.doExecute(this.testRestTemplate);
    }
}