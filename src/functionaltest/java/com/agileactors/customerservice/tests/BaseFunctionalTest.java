package com.agileactors.customerservice.tests;

import com.agileactors.customerservice.common.actors.CustomerServiceActor;
import com.agileactors.customerservice.common.configuration.Configuration;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import(Configuration.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("FUNCTIONAL-TEST")
public abstract class BaseFunctionalTest {

    @Autowired
    public CustomerServiceActor customerServiceActor;
}


