package com.agileactors.customerservice.tests;

import com.agileactors.customerservice.enums.DeveloperLevels;
import com.agileactors.customerservice.models.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomerServiceFunctionalTest extends BaseFunctionalTest {

    @Test
    public void postValidCustomer_withValidData_shouldSucceed() {
        Customer customer = Customer.builder()
                .companyName("yolo")
                .developerLevels(List.of(DeveloperLevels.JUNIOR, DeveloperLevels.MID))
                .build();
        ResponseEntity<String> response = customerServiceActor.postCustomer(customer);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull().isNotEmpty().isNotBlank();

    }

    @Test
    public void postValidCustomer_withoutData_shouldFail() {
        ResponseEntity<String> response = customerServiceActor.postCustomer(null);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNull();

    }

}
