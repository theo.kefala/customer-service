package com.agileactors.customerservice.services;

import com.agileactors.customerservice.exceptions.CustomerServiceException;
import com.agileactors.customerservice.models.Customer;
import com.agileactors.customerservice.repositories.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.agileactors.customerservice.enums.DeveloperLevels.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerService customerService;

    public CustomerServiceTest() {
        this.customerService = new CustomerService();
    }

    @Test
    public void addCustomer_Valid() {
        Customer customer = Customer.builder()
                .companyName("company")
                .developerLevels(List.of(JUNIOR, MID, SENIOR, PRINCIPAL))
                .build();
        String customerId = customerService.addCustomer(customer);
        assertThat(customerId)
                .isNotNull()
                .isNotEmpty()
                .isNotBlank();
    }

    @Test
    public void addCustomer_missingCustomerData_Invalid() {
        assertThatThrownBy(() -> customerService.addCustomer(null))
                .isInstanceOf(CustomerServiceException.class)
                .hasMessageContaining("The customer's data are missing");
    }

    @Test
    public void addCustomer_missingCompanyName_Invalid() {
        Customer customer = Customer.builder()
                .companyName("")
                .developerLevels(List.of(JUNIOR, MID, SENIOR, PRINCIPAL))
                .build();
        assertThatThrownBy(() -> customerService.addCustomer(customer))
                .isInstanceOf(CustomerServiceException.class)
                .hasMessageContaining("The customer's data are missing");
    }

    @Test
    public void addCustomer_missingDeveloperLevels_Invalid() {
        Customer customer = Customer.builder()
                .companyName("company")
                .developerLevels(List.of())
                .build();
        assertThatThrownBy(() -> customerService.addCustomer(customer))
                .isInstanceOf(CustomerServiceException.class)
                .hasMessageContaining("The customer's data are missing");
    }
}
