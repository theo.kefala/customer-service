package com.agileactors.customerservice.controllers;

import com.agileactors.customerservice.services.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    private final static String ID = "12345";

    @Test
    public void createCustomer_Valid() {
        when(customerService.addCustomer(any())).thenReturn(ID);
        String customerId = customerController.createEmployee(any());
        assertThat(customerId).isNotNull();
    }

    @Test
    public void createCustomer_customerNotExist_Invalid() {
        String customerId = customerController.createEmployee(null);
        assertThat(customerId).isNull();
    }
}
