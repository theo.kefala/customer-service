package com.agileactors.customerservice.repositories;

import com.agileactors.customerservice.models.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.agileactors.customerservice.enums.DeveloperLevels.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class CustomerRepositoryTest {

    private final CustomerRepository customerRepository;

    public CustomerRepositoryTest() {
        this.customerRepository = new CustomerRepository();
    }

    @Test
    public void createNewCustomer_Valid() {
        Customer customer = Customer.builder()
                .companyName("yolo")
                .developerLevels(List.of(JUNIOR, MID, SENIOR))
                .build();
        String customerId = customerRepository.save(customer);
        assertThat(customerId).isNotBlank().isNotEmpty().isNotNull();
    }

    @Test
    public void createNewCustomer_Invalid() {
        assertThatThrownBy(() -> customerRepository.save(null))
                .isInstanceOf(NullPointerException.class);
    }
}
